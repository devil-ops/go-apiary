package apiary

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestStatus(t *testing.T) {
	status, err := tc.Status.Status(context.Background())
	require.NoError(t, err)
	require.NotNil(t, status)
	require.Equal(t, 3, len(status.Workers))
	require.Equal(t, "ok", status.HTTP)
}
