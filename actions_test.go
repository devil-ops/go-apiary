package apiary

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestActionsList(t *testing.T) {
	got, err := tc.Action.List(context.Background())
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Greater(t, len(got), 0)
}

func TestActionsGet(t *testing.T) {
	got, err := tc.Action.Get(context.Background(), "fake-uuid")
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestActionsDelete(t *testing.T) {
	got, err := tc.Action.Delete(context.Background(), "fake-uuid")
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestActionsUpdate(t *testing.T) {
	comment := "updated comment"
	got, err := tc.Action.Update(context.Background(), "fake-uuid", ActionOpts{
		Comment: &comment,
	})
	require.NoError(t, err)
	require.NotNil(t, got)
}
