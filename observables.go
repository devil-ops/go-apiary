package apiary

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/google/go-querystring/query"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

const observablePath = "/observables"

var maxConfidence uint = 100

type ObservableService interface {
	List(context.Context, string) ([]*Observable, error)
	Create(context.Context, ObservableCreateOpts) error
	CreateWithFiles(context.Context, ObservableCreateOpts) error
}

type ObservableServiceOp struct {
	client *Client
}

type ObservableCreateOpts struct {
	Metadata ObservableMeta
	Items    []string `fake:"{ipv4address}" fakesize:"1"`
}

func NewObservableCreateOptsWithViper(v *viper.Viper, args []string) (*ObservableCreateOpts, error) {
	action := v.GetString("action")
	var tlp *TLP
	var err error
	if tlp, err = TLPWithString(v.GetString("tlp")); err != nil {
		return nil, err
	}
	vf := v.GetDuration("valid-for")
	confidence := v.GetUint("confidence")
	opts := &ObservableCreateOpts{
		Metadata: ObservableMeta{
			Action:     ActionVerb(action),
			Comment:    v.GetString("comment"),
			Confidence: &confidence,
			Source:     v.GetString("source"),
			TLP:        tlp,
		},
		Items: args,
	}
	if vfs := v.GetDuration("valid-for").Seconds(); vf > 0 {
		vf := int(vfs)
		vfu := uint(vf)
		opts.Metadata.ValidFor = &vfu
	}
	if vfu := v.GetString("valid-until"); vfu != "" {
		t, err := time.Parse("2006-01-02", vfu)
		if err != nil {
			return nil, err
		}
		opts.Metadata.ValidUntil = &t

	}
	if err := opts.Metadata.Validate(); err != nil {
		return nil, err
	}
	log.Debug().Interface("create-opts", opts).Send()
	return opts, nil
}

type ObservableMeta struct {
	Action     ActionVerb `json:"action" url:"action" fake:"{randomstring:[block,allow,watch]}"`
	Comment    string     `json:"comment,omitempty" url:"comment" fake:"{sentence}"`
	Confidence *uint      `json:"confidence" url:"confidence" fake:"{number:0,100}"`
	Source     string     `json:"source" url:"source" fake:"{hipstersentence}"`
	TLP        *TLP       `json:"tlp" url:"tlp"`
	ValidUntil *time.Time `json:"valid_until,omitempty" url:"valid_until,omitempty" fake:"skip"`
	ValidFor   *uint      `json:"valid_for,omitempty" url:"valid_for,omitempty" fake:"{number:0,3153600000}"`
}

func (om *ObservableMeta) Validate() error {
	switch {
	case om.Action == "":
		return errors.New("must include an action")
	case om.Confidence == nil:
		return errors.New("must include a confidence")
	case om.Source == "":
		return errors.New("must include a source")
	case om.TLP == nil:
		return errors.New("must include a tlp")
	case *om.Confidence > maxConfidence:
		return errors.New("confidence is greater than 100")
	case (om.ValidFor != nil) && (om.ValidUntil != nil):
		return errors.New("cannot include both valid_until and valid_for")
	default:
		return nil
	}
}

func (om *ObservableMeta) URLEncode() string {
	v, _ := query.Values(om)
	return v.Encode()
}

func (svc *ObservableServiceOp) CreateWithFiles(ctx context.Context, opts ObservableCreateOpts) error {
	if err := opts.Metadata.Validate(); err != nil {
		return err
	}

	pathArgs := opts.Metadata.URLEncode()

	var (
		buf = new(bytes.Buffer)
		w   = multipart.NewWriter(buf)
	)

	for _, f := range opts.Items {
		fh, err := os.Open(f)
		if err != nil {
			return err
		}
		defer fh.Close()
		ft, err := GetFileContentType(fh)
		if err != nil {
			return err
		}
		part, err := w.CreateFormFile(ft, filepath.Base(fh.Name()))
		if err != nil {
			return err
		}
		_, err = io.Copy(part, fh)
		if err != nil {
			return err
		}
	}
	w.Close()

	// log.Fatal().Interface("buf", buf.String()).Send()
	req, err := http.NewRequest("POST", fmt.Sprintf("%s%s/upload?%s", svc.client.baseURL, observablePath, pathArgs), buf)
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", w.FormDataContentType())
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return err
	}

	return nil
}

func (svc *ObservableServiceOp) Create(ctx context.Context, opts ObservableCreateOpts) error {
	if err := opts.Metadata.Validate(); err != nil {
		return err
	}

	pathArgs := opts.Metadata.URLEncode()

	body, err := json.Marshal(opts.Items)
	if err != nil {
		return err
	}
	req, err := http.NewRequest("POST", fmt.Sprintf("%s%s?%s", svc.client.baseURL, observablePath, pathArgs), bytes.NewReader(body))
	if err != nil {
		return err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return err
	}

	return nil
}

func (svc *ObservableServiceOp) List(ctx context.Context, v string) ([]*Observable, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s?value=%s", svc.client.baseURL, observablePath, v), nil)
	if err != nil {
		return nil, err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}
	b, err := json.Marshal(apiR.Resources)
	if err != nil {
		return nil, err
	}
	var obs []*Observable
	err = json.Unmarshal(b, &obs)
	if err != nil {
		return nil, err
	}
	return obs, nil
}

type Observable struct {
	Action     ActionVerb     `json:"action,omitempty"`
	Comment    string         `json:"comment,omitempty"`
	Confidence int            `json:"confidence,omitempty"`
	ID         string         `json:"id,omitempty"`
	Observable ObservableItem `json:"observable,omitempty"`
	Source     string         `json:"source,omitempty"`
	TLP        TLP            `json:"tlp,omitempty"`
	ValidUntil *time.Time     `json:"valid_until,omitempty"`
}
type ObservableItem struct {
	Type  ObservableType `json:"type,omitempty"`
	Value string         `json:"value,omitempty" fake:"ipv4address"`
}

// NewObservableItem - Create a new Observable Item from a string
func NewObservableItem(s string) (*ObservableItem, error) {
	if !strings.Contains(s, ":") {
		return nil, errors.New("must contain at least one ':'")
	}
	idx := strings.Index(s, ":")

	ts := s[0:idx]
	ot, err := GetObservableType(ts)
	if err != nil {
		return nil, err
	}
	oi := &ObservableItem{
		Type:  *ot,
		Value: s[idx+1:],
	}
	return oi, nil
}

type (
	ObservableType string
)

var (
	ObservableIPv4Addr ObservableType = "ipv4addr"
	ObservableIPv4Net  ObservableType = "ipv4net"
	ObservableDomain   ObservableType = "domain"
	ObservableURL      ObservableType = "url"
)

// Use this map to look up observable types
var observableTypeMap map[string]ObservableType = map[string]ObservableType{
	"ipv4addr": ObservableIPv4Addr,
	"ipv4net":  ObservableIPv4Net,
	"domain":   ObservableDomain,
	"url":      ObservableURL,
}

// GetObservableType - Given a string, return the matching Observable Type
func GetObservableType(s string) (*ObservableType, error) {
	if v, ok := observableTypeMap[s]; ok {
		return &v, nil
	}
	return nil, errors.New("invalid observable type")
}
