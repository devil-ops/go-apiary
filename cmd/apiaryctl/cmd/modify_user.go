/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.oit.duke.edu/devil-ops/go-apiary"
)

// modifyUserCmd represents the modifyUser command
var modifyUserCmd = &cobra.Command{
	Use:   "user UUID [--flags]",
	Short: "Modify an existing user",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		opts, err := apiary.NewUserUpdateOptsWithViper(viper.GetViper())
		checkErr(err, "")
		log.Debug().Interface("opts", opts).Msg("Options")
		nu, err := client.User.Update(context.Background(), args[0], *opts)
		checkErr(err, "Error creating user")
		outter.MustPrint(nu)
	},
}

func init() {
	modifyCmd.AddCommand(modifyUserCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// modifyUserCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// modifyUserCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	modifyUserCmd.PersistentFlags().StringP("email", "e", "", "Email for user")
	modifyUserCmd.PersistentFlags().StringP("password", "p", "", "Password for user")
	modifyUserCmd.PersistentFlags().BoolP("admin", "a", false, "Should be Admin?")
	modifyUserCmd.PersistentFlags().BoolP("read-only", "r", false, "Should be Read-Only?")
	modifyUserCmd.PersistentFlags().BoolP("disabled", "d", false, "Should be Disabled?")
	modifyUserCmd.PersistentFlags().StringP("max-tlp", "m", "", "Maximum TLP")
}
