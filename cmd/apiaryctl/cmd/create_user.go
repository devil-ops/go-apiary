/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.oit.duke.edu/devil-ops/go-apiary"
)

// createUserCmd represents the createUser command
var createUserCmd = &cobra.Command{
	Use:   "user USERNAME",
	Short: "Create a new Apiary user",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		opts, err := apiary.NewUserAddOptsWithViper(viper.GetViper(), args[0])
		checkErr(err, "")
		log.Debug().Interface("opts", opts).Msg("Options")
		nu, err := client.User.Create(context.Background(), *opts)
		checkErr(err, "Error creating user")
		outter.MustPrint(nu)
	},
}

func init() {
	createCmd.AddCommand(createUserCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	createUserCmd.PersistentFlags().StringP("email", "e", "", "Email for user")
	createUserCmd.PersistentFlags().StringP("password", "p", "", "Password for user")
	createUserCmd.PersistentFlags().BoolP("admin", "a", false, "Create user as Admin")
	createUserCmd.PersistentFlags().BoolP("read-only", "r", false, "Create user as Read-Only")
	createUserCmd.PersistentFlags().BoolP("disabled", "d", false, "Create user as Disabled")
	createUserCmd.PersistentFlags().StringP("max-tlp", "m", "", "Create user with the given maximum TLP")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// createUserCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
