/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.oit.duke.edu/devil-ops/go-apiary"
)

// createCollectionCmd represents the createCollection command
var createCollectionCmd = &cobra.Command{
	Use:   "collection",
	Short: "Add a new collection",
	Run: func(cmd *cobra.Command, args []string) {
		opts, err := apiary.NewCollectionAddOptsWithViper(viper.GetViper())
		checkErr(err, "")
		log.Debug().Interface("opts", opts).Msg("Options")
		i, err := client.Collection.Create(context.Background(), *opts)
		checkErr(err, "Error creating collection")
		outter.MustPrint(i)
	},
}

func init() {
	createCmd.AddCommand(createCollectionCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// createCollectionCmd.PersistentFlags().String("foo", "", "A help for foo")
	createCollectionCmd.PersistentFlags().StringArray("source", []string{}, HelpCollectionSource)
	createCollectionCmd.PersistentFlags().StringArray("pattern", []string{}, HelpCollectionPattern)
	createCollectionCmd.PersistentFlags().StringArray("owner", []string{}, HelpCollectionOwner)
	createCollectionCmd.PersistentFlags().StringArray("subscriber", []string{}, HelpCollectionSubscriber)
	createCollectionCmd.PersistentFlags().Int("minimum-confidence", 0, HelpCollectionConfidence)
	createCollectionCmd.PersistentFlags().String("comment", "", HelpComment)
	createCollectionCmd.PersistentFlags().String("tlp", "", HelpTLP)
	createCollectionCmd.MarkPersistentFlagRequired("tlp")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// createCollectionCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
