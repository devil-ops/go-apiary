package cmd

var (
	HelpTLP                  string = "The TLP of this action, reference https://www.first.org/tlp/"
	HelpComment              string = "Optional comment for further information or reference"
	HelpSource               string = "The source of this action. This field should be specific and consistent, as collections will pull actions from sources that match specific regexes."
	HelpConfidence           string = "The confidence in this action, between 0 (no confidence) and 100 (full confidence)"
	HelpAction               string = "What this action is meant to do"
	HelpValidFor             string = "The duration for which this action is valid, cannot be specified if valid-until is also provided"
	HelpValidUntil           string = "The time until which this action is valid, defaults to the year 2500 unless valid_for is provided"
	HelpObservable           string = "The thing that is going to be observed"
	HelpCollectionPattern    string = "A list of case-insensitive regex patterns to search for matching action sources to be included in this collection"
	HelpCollectionSource     string = "A list of case-insensitive strings to search for matching action sources to be included in this collection"
	HelpCollectionOwner      string = "A list of users by UUID who have permission to modify this collection, defaults to just the creator"
	HelpCollectionSubscriber string = "A list of subscribing outputs by UUID which need to be informed when this collection changes"
	HelpCollectionConfidence string = "The minimum confidence an Action must have before being included in this collection, with values between 0 (no confidence) and 100 (full confidence)"
	HelpCollectionTLP        string = "The TLP of this collection, reference https://www.first.org/tlp/"
)
