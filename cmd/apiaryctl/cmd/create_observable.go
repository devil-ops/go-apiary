/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.oit.duke.edu/devil-ops/go-apiary"
)

// createObservableCmd represents the createObservable command
var createObservableCmd = &cobra.Command{
	Use:     "observable [--flags] LIST_OF_OBSERVERABLES|LIST_OF_FILES",
	Short:   "Create an Observable",
	Args:    cobra.MinimumNArgs(1),
	Aliases: []string{"observables", "o"},
	Run: func(cmd *cobra.Command, args []string) {
		// All the metadata here
		opts, err := apiary.NewObservableCreateOptsWithViper(viper.GetViper(), args)
		checkErr(err, "Could not get viper opts for observable")

		var creater func(context.Context, apiary.ObservableCreateOpts) error
		if viper.GetBool("files") {
			creater = client.Observable.CreateWithFiles
		} else {
			creater = client.Observable.Create
		}
		err = creater(context.Background(), *opts)
		checkErr(err, "")
		log.Info().Msg("Created! 👍")
	},
}

func init() {
	createCmd.AddCommand(createObservableCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// createObservableCmd.PersistentFlags().String("foo", "", "A help for foo")
	createObservableCmd.PersistentFlags().String("tlp", "", HelpTLP)
	createObservableCmd.PersistentFlags().String("action", "", HelpAction)
	createObservableCmd.PersistentFlags().String("source", "", HelpSource)
	createObservableCmd.PersistentFlags().String("comment", "", HelpComment)
	createObservableCmd.PersistentFlags().Duration("valid-for", 0, HelpValidFor)
	createObservableCmd.PersistentFlags().String("valid-until", "", HelpValidUntil)

	createObservableCmd.PersistentFlags().Bool("files", false, "THIS NO WORKY ON HERE YET - Instead of listing observable values, upload files containing those values")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// createObservableCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
