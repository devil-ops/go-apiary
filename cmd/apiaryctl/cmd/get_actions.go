/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"

	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-apiary"
)

// getActionCmd represents the getAction command
var getActionCmd = &cobra.Command{
	Use:     "action",
	Short:   "List actions",
	Aliases: []string{"actions", "a"},
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			res, err := client.Action.List(context.Background())
			checkErr(err, "Error getting actions")
			outter.MustPrint(res)
		} else {
			actions := make([]*apiary.Action, len(args))
			for idx, id := range args {
				action, err := client.Action.Get(context.Background(), id)
				checkErr(err, "")
				actions[idx] = action
			}
			outter.MustPrint(actions)
		}
	},
}

func init() {
	getCmd.AddCommand(getActionCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getActionCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getActionCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
