/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"
	"sync"
	"time"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-apiary"
)

// loadtestCmd represents the loadtest command
var loadtestCmd = &cobra.Command{
	Use:   "loadtest",
	Short: "Generate lots of fake data to load test Apiary",
	Long:  "WARNING: This will write a bunch of garbage data to your server, don't do it in production",
	Run: func(cmd *cobra.Command, args []string) {
		oc, err := cmd.PersistentFlags().GetInt("observable-count")
		checkErr(err, "")

		maxConcurrent, err := cmd.PersistentFlags().GetInt("max-concurrent")
		guard := make(chan struct{}, maxConcurrent)

		var wg sync.WaitGroup
		wg.Add(oc)
		for i := 0; i < oc; i++ {
			guard <- struct{}{}
			go func() {
				defer wg.Done()
				o := apiary.ObservableCreateOpts{}
				err = gofakeit.Struct(&o)
				checkErr(err, "")

				log.Debug().Interface("opt", o).Send()
				err = client.Observable.Create(context.Background(), o)
				checkErr(err, "")
				<-guard
			}()
		}
		wg.Wait()

		totalTime := time.Since(started)
		ips := float64(oc) / totalTime.Seconds()
		log.Info().Float64("items-per-second", ips).Msg("Items sent per second")
	},
}

func init() {
	rootCmd.AddCommand(loadtestCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// loadtestCmd.PersistentFlags().String("foo", "", "A help for foo")
	loadtestCmd.PersistentFlags().Int("observable-count", 100, "Number of observables to test during load test")
	loadtestCmd.PersistentFlags().Int("max-concurrent", 5, "Number of concurrent processes to run")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// loadtestCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
