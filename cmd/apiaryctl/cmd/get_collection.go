/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"

	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-apiary"
)

// getCollectionCmd represents the getCollection command
var getCollectionCmd = &cobra.Command{
	Use:   "collection",
	Short: "Get Collections",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			res, err := client.Collection.List(context.Background())
			checkErr(err, "Error getting collections")
			outter.MustPrint(res)
		} else {
			cs := make([]*apiary.Collection, len(args))
			for idx, item := range args {
				c, err := client.Collection.Get(context.Background(), item)
				checkErr(err, "")
				cs[idx] = c
			}
			outter.MustPrint(cs)
		}
	},
}

func init() {
	getCmd.AddCommand(getCollectionCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getCollectionCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getCollectionCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
