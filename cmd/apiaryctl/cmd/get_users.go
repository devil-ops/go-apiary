/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"

	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-apiary"
)

// getUsersCmd represents the getUsers command
var getUsersCmd = &cobra.Command{
	Use:     "users",
	Short:   "Get users from the API",
	Aliases: []string{"u", "user"},
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			res, err := client.User.List(context.Background())
			checkErr(err, "Error listing users")
			outter.MustPrint(res)
		} else {
			users := []*apiary.User{}
			for _, id := range args {
				u, err := client.User.Get(context.Background(), id)
				checkErr(err, "")
				users = append(users, u)
			}
			outter.MustPrint(users)
		}
	},
}

func init() {
	getCmd.AddCommand(getUsersCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getUsersCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getUsersCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
