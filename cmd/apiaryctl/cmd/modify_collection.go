/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.oit.duke.edu/devil-ops/go-apiary"
)

// modifyCollectionCmd represents the modifyCollection command
var modifyCollectionCmd = &cobra.Command{
	Use:   "collection UUID",
	Short: "Modify a collection by it's UUID",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		opts, err := apiary.NewCollectionUpdateOptsWithViper(viper.GetViper())
		checkErr(err, "")
		log.Debug().Interface("opts", opts).Msg("Options")
		i, err := client.Collection.Update(context.Background(), args[0], *opts)
		checkErr(err, "Error modifying collection")
		outter.MustPrint(i)
	},
}

func init() {
	modifyCmd.AddCommand(modifyCollectionCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// modifyCollectionCmd.PersistentFlags().String("foo", "", "A help for foo")
	modifyCollectionCmd.PersistentFlags().StringArray("source", []string{}, HelpCollectionSource)
	modifyCollectionCmd.PersistentFlags().StringArray("pattern", []string{}, HelpCollectionPattern)
	modifyCollectionCmd.PersistentFlags().StringArray("owner", []string{}, HelpCollectionOwner)
	modifyCollectionCmd.PersistentFlags().StringArray("subscriber", []string{}, HelpCollectionSubscriber)
	modifyCollectionCmd.PersistentFlags().Int("minimum-confidence", 0, HelpCollectionConfidence)
	modifyCollectionCmd.PersistentFlags().String("comment", "", HelpComment)
	modifyCollectionCmd.PersistentFlags().String("tlp", "", HelpTLP)
	modifyCollectionCmd.MarkPersistentFlagRequired("tlp")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// modifyCollectionCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
