/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/drewstinnett/go-output-format/v2/gout"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-apiary"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var (
	cfgFile string
	client  *apiary.Client
	Verbose bool
	version = "dev"
	outter  *gout.Client
	started time.Time
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:     "apiaryctl",
	Short:   "Interact with Apiary on the CLI",
	Version: version,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		started = time.Now()
		var err error
		err = viper.BindPFlags(cmd.Flags())
		checkErr(err, "")
		outter, err = gout.NewWithCobraCmd(cmd, &gout.CobraCmdConfig{
			FormatField: "output-format",
		})
		checkErr(err, "Could not output formatter")

		client, err = apiary.NewClientWithViper(viper.GetViper(), nil)
		checkErr(err, "Could not create client")
	},
	PersistentPostRun: func(cmd *cobra.Command, args []string) {
		totalT := time.Since(started)
		log.Info().Str("took", totalT.String()).Msg("🐝")
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.apiaryctl.yaml)")
	rootCmd.PersistentFlags().String("url", "http://localhost:8000", "URL for Apiary service")
	rootCmd.PersistentFlags().String("auth-username", "", "Username for authentication")
	rootCmd.PersistentFlags().String("auth-password", "", "Password for authentication")
	rootCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "Verbose output")
	rootCmd.PersistentFlags().StringP("output-format", "o", "yaml", "Format of output where applicable (yaml|json|toml|csv)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".apiaryctl" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".apiaryctl")
	}

	viper.SetEnvPrefix("APIARY")
	viper.AutomaticEnv() // read in environment variables that match

	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if Verbose {
		log.Logger = log.Logger.With().Caller().Logger()
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}

func checkErr(err error, msg string) {
	if msg == "" {
		msg = "Fatal Error"
	}
	if err != nil {
		log.Fatal().Err(err).Msg(msg)
	}
}
