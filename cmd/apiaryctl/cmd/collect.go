/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
)

// collectCmd represents the collect command
var collectCmd = &cobra.Command{
	Use:   "collect COLLECTION-UUID ACTION",
	Short: "Collect observables",
	Args:  cobra.ExactArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		pt, err := cmd.PersistentFlags().GetBool("plain-text")
		checkErr(err, "")

		if pt {
			data, err := client.Collection.CollectPlainText(context.Background(), args[0], args[1])
			checkErr(err, "")
			fmt.Printf(string(data))
		} else {
			data, err := client.Collection.Collect(context.Background(), args[0], args[1])
			checkErr(err, "")
			outter.MustPrint(data)
		}
	},
}

func init() {
	rootCmd.AddCommand(collectCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// collectCmd.PersistentFlags().String("foo", "", "A help for foo")
	collectCmd.PersistentFlags().Bool("plain-text", false, "Use plain text output")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// collectCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
