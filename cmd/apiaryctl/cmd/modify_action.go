/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.oit.duke.edu/devil-ops/go-apiary"
)

// modifyActionCmd represents the modifyAction command
var modifyActionCmd = &cobra.Command{
	Use:   "action UUID",
	Short: "Update the details of a specific action by its UUID",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		opts, err := apiary.NewActionOptsWithViper(viper.GetViper())
		checkErr(err, "")
		log.Debug().Interface("opts", opts).Msg("modify action options")
		nu, err := client.Action.Update(context.Background(), args[0], *opts)
		checkErr(err, "Error modifying action")
		outter.MustPrint(nu)
	},
}

func init() {
	modifyCmd.AddCommand(modifyActionCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// modifyActionCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	modifyActionCmd.PersistentFlags().String("comment", "", HelpComment)
	modifyActionCmd.PersistentFlags().String("source", "", HelpSource)
	modifyActionCmd.PersistentFlags().String("action", "", HelpAction)
	modifyActionCmd.PersistentFlags().String("observable", "", HelpObservable)
	modifyActionCmd.PersistentFlags().Int("confidence", -1, HelpConfidence)
	modifyActionCmd.PersistentFlags().String("tlp", "", HelpTLP)
	modifyActionCmd.PersistentFlags().Duration("valid-for", 0, HelpValidFor)
	modifyActionCmd.PersistentFlags().String("valid-until", "", HelpValidUntil)

	/*
		createObservableCmd.PersistentFlags().String("action", "", "What this action is meant to do")
		createObservableCmd.PersistentFlags().Duration("valid-for", 0, "The duration for which this action is valid, cannot be specified if valid-until is also provided")
		createObservableCmd.PersistentFlags().String("valid-until", "", "The time until which this action is valid, defaults to the year 2500 unless valid_for is provided")
	*/
	// modifyActionCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
