package apiary

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestUsersWhoAmI(t *testing.T) {
	me, err := tc.User.WhoAmI(context.Background())
	require.NoError(t, err)
	require.NotNil(t, me)
	require.Equal(t, "admin", me.Username)
	require.True(t, me.Admin)
}

func TestUsersList(t *testing.T) {
	users, err := tc.User.List(context.Background())
	require.NoError(t, err)
	require.NotNil(t, users)
	require.Greater(t, len(users), 0)
}

func TestUsersAPIKey(t *testing.T) {
	key, err := tc.User.APIKey(context.Background())
	require.NoError(t, err)
	require.Equal(t, "my-fancy-key", key)
}

func TestUserCreate(t *testing.T) {
	got, err := tc.User.Create(context.Background(), UserAddOpts{
		Username: "boomhaur",
	})
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestUserModify(t *testing.T) {
	got, err := tc.User.Update(context.Background(), "fake-user-uuid", UserAddOpts{
		Username: "boomhaur",
	})
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestUserGet(t *testing.T) {
	got, err := tc.User.Get(context.Background(), "fake-user-uuid")
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestUserDelete(t *testing.T) {
	got, err := tc.User.Delete(context.Background(), "fake-user-uuid")
	require.NoError(t, err)
	require.NotNil(t, got)
}
