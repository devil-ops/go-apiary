package main

import (
	"context"
	"fmt"
	"os"

	"gitlab.oit.duke.edu/devil-ops/go-apiary"
)

func main() {
	if len(os.Args) != 4 {
		panic(fmt.Sprintf("Usage: %v URL USERNAME PASSWORD", os.Args[0]))
	}

	client, err := apiary.NewClient(apiary.ClientConfig{
		BaseURL: os.Args[1],
		Auth: apiary.AuthOpts{
			Username: os.Args[2],
			Password: os.Args[3],
		},
	}, nil)
	if err != nil {
		panic(err)
	}

	user, err := client.User.WhoAmI(context.Background())
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v\n", user)
}
