package apiary

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

func TestTLPMarshal(t *testing.T) {
	b, err := json.Marshal(TLPAmber)
	require.NoError(t, err)
	require.Equal(t, `"amber"`, string(b))
}

func TestTLPMarshalYAML(t *testing.T) {
	b, err := yaml.Marshal(TLPAmber)
	require.NoError(t, err)
	require.Equal(t, "amber\n", string(b))
}

func TestTLPUnmarshal(t *testing.T) {
	var got TLP
	err := json.Unmarshal([]byte(`"amber"`), &got)
	require.NoError(t, err)
	require.Equal(t, TLPAmber, got)
}
