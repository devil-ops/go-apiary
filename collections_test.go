package apiary

import (
	"context"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"
)

func TestCollectionList(t *testing.T) {
	got, err := tc.Collection.List(context.Background())
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestCollectionCreate(t *testing.T) {
	got, err := tc.Collection.Create(context.Background(), CollectionCreateOpts{})
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestCollectionGet(t *testing.T) {
	got, err := tc.Collection.Get(context.Background(), "fake-collection-uuid")
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestCollectionDelete(t *testing.T) {
	got, err := tc.Collection.Delete(context.Background(), "fake-collection-uuid")
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestCollectionUpdate(t *testing.T) {
	got, err := tc.Collection.Update(context.Background(), "fake-collection-uuid", CollectionUpdateOpts{})
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestNewCollectionUpdateOptsWithViper(t *testing.T) {
	v := viper.New()
	v.Set("tlp", "amber")
	got, err := NewCollectionUpdateOptsWithViper(v)
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Equal(t, "amber", got.TLP)
}

func TestCollectionCollectTXT(t *testing.T) {
	got, err := tc.Collection.CollectPlainText(context.Background(), "fake-collection-uuid", "block")
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Equal(t, `192.0.2.50
192.0.2.0/24
www.example.com
https://www.example.com/example.exe
`, string(got))
}

func TestCollectionCollect(t *testing.T) {
	got, err := tc.Collection.Collect(context.Background(), "fake-collection-uuid", "block")
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Greater(t, len(got), 0)
}
