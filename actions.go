package apiary

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/spf13/viper"
)

const actionPath = "/actions"

type ActionVerb string

var (
	ActionAllow   ActionVerb = "allow"
	ActionBlock   ActionVerb = "block"
	ActionWatch   ActionVerb = "watch"
	ActionUnknown ActionVerb = "unknown"
)

func ActionVerbWithString(s string) ActionVerb {
	switch s {
	case "allow":
		return ActionAllow
	case "block":
		return ActionBlock
	case "watch":
		return ActionWatch
	default:
		return ActionUnknown
	}
}

type ActionService interface {
	List(context.Context) ([]*Action, error)
	Get(context.Context, string) (*Action, error)
	Delete(context.Context, string) (*Action, error)
	Update(context.Context, string, ActionOpts) (*Action, error)
}

type ActionServiceOp struct {
	client *Client
}

func actionsWithInterface(v interface{}) ([]*Action, error) {
	items := v.([]interface{})
	actions := make([]*Action, len(items))
	// TODO: This seems non-optimal...need a better way to handle this
	for idx, item := range items {
		action, err := actionWithInterface(item)
		if err != nil {
			return nil, err
		}
		actions[idx] = action
	}
	return actions, nil
}

func actionWithInterface(v interface{}) (*Action, error) {
	b, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}
	var action *Action
	err = json.Unmarshal(b, &action)
	if err != nil {
		return nil, err
	}
	return action, nil
	/*
		DELETE THIS PROLLY
			a := v.(map[string]interface{})
			obs := a["observable"].(map[string]interface{})
			tlp, err := TLPWithString(a["tlp"].(string))
			if err != nil {
				return nil, err
			}
			timeR := a["valid_until"].(string)
			timeT, err := time.Parse("2006-01-02T03:04:00+04:00", timeR)
			if err != nil {
				return nil, err
			}
			action := &Action{
				Action:     a["action"].(string),
				Comment:    a["comment"].(string),
				Confidence: int(a["confidence"].(float64)),
				ID:         a["id"].(string),
				Observable: ObservableItem{
					Type:  obs["type"].(string),
					Value: obs["value"].(string),
				},
				Source:     a["source"].(string),
				ValidUntil: timeT,
				TLP:        *tlp,
			}
			return action, nil
	*/
}

func (svc *ActionServiceOp) Update(ctx context.Context, id string, opts ActionOpts) (*Action, error) {
	body, err := json.Marshal(opts)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("PATCH", fmt.Sprintf("%s%s/%s", svc.client.baseURL, actionPath, id), bytes.NewReader(body))
	if err != nil {
		return nil, err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}
	action, err := actionWithInterface(apiR.Resources)
	if err != nil {
		return nil, err
	}
	return action, nil
}

func (svc *ActionServiceOp) Delete(ctx context.Context, id string) (*Action, error) {
	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s%s/%s", svc.client.baseURL, actionPath, id), nil)
	if err != nil {
		return nil, err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}
	action, err := actionWithInterface(apiR.Resources)
	if err != nil {
		return nil, err
	}
	return action, nil
}

func (svc *ActionServiceOp) Get(ctx context.Context, id string) (*Action, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s/%s", svc.client.baseURL, actionPath, id), nil)
	if err != nil {
		return nil, err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}
	action, err := actionWithInterface(apiR.Resources)
	if err != nil {
		return nil, err
	}
	return action, nil
}

func (svc *ActionServiceOp) List(ctx context.Context) ([]*Action, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.baseURL, actionPath), nil)
	if err != nil {
		return nil, err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}

	actions, err := actionsWithInterface(apiR.Resources)
	if err != nil {
		return nil, err
	}

	return actions, nil
}

type Action struct {
	Action     string         `json:"action,omitempty"`
	Comment    string         `json:"comment,omitempty"`
	Confidence int            `json:"confidence,omitempty"`
	ID         string         `json:"id,omitempty"`
	Observable ObservableItem `json:"observable,omitempty"`
	Source     string         `json:"source,omitempty"`
	TLP        TLP            `json:"tlp,omitempty"`
	ValidUntil time.Time      `json:"valid_until,omitempty"`
}

type ActionOpts struct {
	Action     *ActionVerb     `json:"action,omitempty"`
	Comment    *string         `json:"comment,omitempty"`
	Confidence *uint           `json:"confidence,omitempty"`
	Observable *ObservableItem `json:"observable,omitempty"`
	Source     *string         `json:"source,omitempty"`
	TLP        *TLP            `json:"tlp,omitempty"`
	ValidUntil *time.Time      `json:"valid_until,omitempty"`
	ValidFor   *int            `json:"valid_for,omitempty"`
}

func NewActionOptsWithViper(v *viper.Viper) (*ActionOpts, error) {
	opts := &ActionOpts{}
	tlp := v.GetString("tlp")
	var err error
	if tlp != "" {
		opts.TLP, err = TLPWithString(tlp)
		if err != nil {
			return nil, err
		}
	}

	obsV := v.GetString("observable")
	if obsV != "" {
		obs, err := NewObservableItem(obsV)
		if err != nil {
			return nil, err
		}
		opts.Observable = obs
	}
	comment := v.GetString("comment")
	if comment != "" {
		opts.Comment = &comment
	}
	source := v.GetString("source")
	if source != "" {
		opts.Source = &source
	}

	action := v.GetString("action")
	if action != "" {
		a := ActionVerbWithString(action)
		opts.Action = &a
	}

	// < 0 == unset
	confidence := v.GetInt("confidence")
	if confidence >= 0 {
		uc := uint(confidence)
		opts.Confidence = &uc
	}

	if vfs := v.GetDuration("valid-for").Seconds(); vfs > 0 {
		vf := int(vfs)
		opts.ValidFor = &vf
	}
	/*
		if vfu := v.GetString("valid-until"); vfu != "" {
			log.Warn().Msg("DING")
			t, err := time.Parse("2006-01-02", vfu)
			if err != nil {
				return nil, err
			}
			opts.ValidUntil = &t

		}
	*/
	return opts, nil
}
