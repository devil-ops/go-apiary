package apiary

import (
	"context"
	"fmt"
	"net/http"

	"github.com/mitchellh/mapstructure"
)

const statusPath = "/status"

type StatusService interface {
	Status(context.Context) (*Status, error)
}

type StatusServiceOp struct {
	client *Client
}

func (svc *StatusServiceOp) Status(context.Context) (*Status, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/status", svc.client.baseURL), nil)
	if err != nil {
		return nil, err
	}
	var apiR APIResponse
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}

	var s Status
	err = mapstructure.Decode(apiR.Resources, &s)
	if err != nil {
		return nil, err
	}
	return &s, nil
}

type Status struct {
	HTTP    string         `json:"http,omitempty"`
	MongoDB string         `json:"mongodb,omitempty"`
	Redis   string         `json:"redis,omitempty"`
	Workers []WorkerStatus `json:"workers,omitempty"`
}

type WorkerStatus struct {
	Name   string   `json:"name,omitempty"`
	Queues []string `json:"queues,omitempty"`
	State  string   `json:"state,omitempty"`
}
