package apiary

import (
	"encoding/json"
	"fmt"
	"net/url"
)

type TLP struct {
	Weight int    `json:"weight" yaml:"weight"`
	Label  string `json:"label" yaml:"label" fake:"{randomstring:[clear,green,amber,amber+strict,red]}"`
}

var (
	TLPClear       TLP = TLP{Weight: 1, Label: "clear"}
	TLPGreen       TLP = TLP{Weight: 1, Label: "green"}
	TLPAmber       TLP = TLP{Weight: 1, Label: "amber"}
	TLPAmberStrict TLP = TLP{Weight: 1, Label: "strict"}
	TLPRed         TLP = TLP{Weight: 1, Label: "red"}
)

func (t *TLP) EncodeValues(key string, values *url.Values) error {
	values.Set(key, t.Label)
	return nil
}

func (tlp TLP) MarshalJSON() ([]byte, error) {
	return json.Marshal(tlp.Label)
}

func (tlp TLP) MarshalYAML() (interface{}, error) {
	return fmt.Sprintf("%v", tlp.Label), nil
}

func (tlp *TLP) UnmarshalJSON(data []byte) error {
	var v interface{}
	err := json.Unmarshal(data, &v)
	if err != nil {
		return err
	}
	new, err := TLPWithString(v.(string))
	if err != nil {
		return err
	}
	tlp.Label = new.Label
	tlp.Weight = new.Weight
	return nil
}

func TLPWithString(s string) (*TLP, error) {
	switch s {
	case "clear":
		return &TLPClear, nil
	case "green":
		return &TLPGreen, nil
	case "amber":
		return &TLPAmber, nil
	case "amber+strict":
		return &TLPAmberStrict, nil
	case "red":
		return &TLPRed, nil
	default:
		return nil, fmt.Errorf("invalid TLP: '%v'", s)
	}
}

func (tlp TLP) String() string {
	switch tlp {
	case TLPClear:
		return "clear"
	case TLPGreen:
		return "green"
	case TLPAmber:
		return "amber"
	case TLPAmberStrict:
		return "amber+strict"
	case TLPRed:
		return "red"
	}
	return ""
}
