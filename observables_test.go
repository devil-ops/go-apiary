package apiary

import (
	"context"
	"testing"
	"time"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestObservableList(t *testing.T) {
	got, err := tc.Observable.List(context.Background(), "foo")
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Greater(t, len(got), 0)
}

func TestObservableCreate(t *testing.T) {
	conf := uint(55)
	err := tc.Observable.Create(context.Background(), ObservableCreateOpts{
		Metadata: ObservableMeta{
			Action:     ActionBlock,
			Confidence: &conf,
			Source:     "foo",
			// TLP:        TLPAmber.Label,
			TLP: &TLPAmber,
		},
		Items: []string{
			"1.2.3.4",
			"https://foo.com",
		},
	})
	require.NoError(t, err)
}

func TestObservableCreateWithFiles(t *testing.T) {
	conf := uint(55)
	err := tc.Observable.CreateWithFiles(context.Background(), ObservableCreateOpts{
		Metadata: ObservableMeta{
			Action:     ActionBlock,
			Confidence: &conf,
			Source:     "foo",
			TLP:        &TLPAmber,
		},
		Items: []string{
			"./testdata/observables/file1.txt",
			"./testdata/observables/file2.txt",
		},
	})
	require.NoError(t, err)
}

func TestObservableValidate(t *testing.T) {
	conf := uint(5)
	badConf := uint(101)
	vf := uint(60)
	vu := time.Now().Add(time.Hour * 5)
	tests := map[string]struct {
		opts    ObservableMeta
		wantErr string
	}{
		"missing-action": {
			opts:    ObservableMeta{},
			wantErr: "must include an action",
		},
		"missing-confidence": {
			opts: ObservableMeta{
				Action: "block",
			},
			wantErr: "must include a confidence",
		},
		"missing-source": {
			opts: ObservableMeta{
				Action:     "block",
				Confidence: &conf,
			},
			wantErr: "must include a source",
		},
		"missing-tlp": {
			opts: ObservableMeta{
				Action:     "block",
				Confidence: &conf,
				Source:     "foo",
			},
			wantErr: "must include a tlp",
		},
		"both-time-filters": {
			opts: ObservableMeta{
				Action:     "block",
				Confidence: &conf,
				Source:     "foo",
				TLP:        &TLPAmber,
				ValidUntil: &vu,
				ValidFor:   &vf,
			},
			wantErr: "cannot include both valid_until and valid_for",
		},
		"invalid-confidence": {
			opts: ObservableMeta{
				Action:     "block",
				Confidence: &badConf,
				Source:     "foo",
				TLP:        &TLPAmber,
			},
			wantErr: "confidence is greater than 100",
		},
		"valid": {
			opts: ObservableMeta{
				Action:     "block",
				Confidence: &conf,
				Source:     "foo",
				TLP:        &TLPAmber,
			},
			wantErr: "",
		},
	}
	for desc, tt := range tests {
		err := tt.opts.Validate()
		if tt.wantErr != "" {
			assert.Error(t, err, desc)
			assert.EqualError(t, err, tt.wantErr, desc)
		} else {
			assert.NoError(t, err, desc)
		}

	}
}

func TestNewObservableCreateOptsWithViper(t *testing.T) {
	v := viper.New()
	v.Set("action", "block")
	v.Set("tlp", "amber")
	v.Set("source", "Twitter")
	v.Set("valid-for", time.Duration(time.Hour*5))

	got, err := NewObservableCreateOptsWithViper(v, []string{"foo", "bar"})
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestNewObservableItem(t *testing.T) {
	tests := map[string]struct {
		s       string
		want    *ObservableItem
		wantErr string
	}{
		"missing-colon": {
			s:       "foo",
			want:    nil,
			wantErr: "must contain at least one ':'",
		},
		"good": {
			s: "ipv4addr:1.2.3.4",
			want: &ObservableItem{
				Type:  "ipv4addr",
				Value: "1.2.3.4",
			},
		},
	}
	for desc, tt := range tests {
		got, err := NewObservableItem(tt.s)
		if tt.wantErr != "" {
			require.Error(t, err, desc)
			require.Nil(t, got, desc)
			require.EqualError(t, err, tt.wantErr, desc)
		} else {
			require.NoError(t, err, desc)
			require.NotNil(t, got, desc)
		}
	}
}

func TestGetObservableType(t *testing.T) {
	got, err := GetObservableType("ipv4net")
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestGetObservableTypeErr(t *testing.T) {
	got, err := GetObservableType("never-exist")
	require.Nil(t, got)
	require.EqualError(t, err, "invalid observable type")
}
