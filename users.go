package apiary

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
)

const userPath = "/users"

type UserService interface {
	List(context.Context) ([]*User, error)
	Get(context.Context, string) (*User, error)
	Delete(context.Context, string) (*User, error)
	Create(context.Context, UserAddOpts) (*User, error)
	Update(context.Context, string, UserAddOpts) (*User, error)
	WhoAmI(context.Context) (*User, error)
	APIKey(context.Context) (string, error)
}

type UserServiceOp struct {
	client *Client
}

type UserAddOpts struct {
	Username string `json:"username,omitempty"`
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
	Admin    bool   `json:"admin,omitempty"`
	ReadOnly bool   `json:"read_only,omitempty"`
	Disabled bool   `json:"disabled,omitempty"`
	TLP      TLP    `json:"max_tlp,omitempty"`
}

func NewUserUpdateOptsWithViper(v *viper.Viper) (*UserAddOpts, error) {
	c := UserAddOpts{
		Username: v.GetString("username"),
		Email:    v.GetString("email"),
		Password: v.GetString("password"),
		Admin:    v.GetBool("admin"),
		ReadOnly: v.GetBool("read-only"),
		Disabled: v.GetBool("disabled"),
	}
	rawTLP := v.GetString("max-tlp")
	if rawTLP != "" {
		tlp, err := TLPWithString(rawTLP)
		if err != nil {
			return nil, err
		}
		c.TLP = *tlp
	}
	return &c, nil
}

func NewUserAddOptsWithViper(v *viper.Viper, username string) (*UserAddOpts, error) {
	if username == "" {
		return nil, errors.New("username must not be empty")
	}
	c := UserAddOpts{
		Username: username,
		Email:    v.GetString("email"),
		Password: v.GetString("password"),
		Admin:    v.GetBool("admin"),
		ReadOnly: v.GetBool("read-only"),
		Disabled: v.GetBool("disabled"),
	}
	rawTLP := v.GetString("max-tlp")
	if rawTLP != "" {
		tlp, err := TLPWithString(rawTLP)
		if err != nil {
			return nil, err
		}
		c.TLP = *tlp
	}
	return &c, nil
}

func (svc *UserServiceOp) Delete(ctx context.Context, id string) (*User, error) {
	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s%s/%s", svc.client.baseURL, userPath, id), nil)
	if err != nil {
		return nil, err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}

	var user *User
	err = mapstructure.Decode(apiR.Resources, &user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (svc *UserServiceOp) Get(ctx context.Context, id string) (*User, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s/%s", svc.client.baseURL, userPath, id), nil)
	if err != nil {
		return nil, err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}

	var user *User
	err = mapstructure.Decode(apiR.Resources, &user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (svc *UserServiceOp) Update(ctx context.Context, id string, opts UserAddOpts) (*User, error) {
	body, err := json.Marshal(opts)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("PATCH", fmt.Sprintf("%s%s/%s", svc.client.baseURL, userPath, id), bytes.NewReader(body))
	if err != nil {
		return nil, err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}

	var user *User
	err = mapstructure.Decode(apiR.Resources, &user)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (svc *UserServiceOp) Create(ctx context.Context, opts UserAddOpts) (*User, error) {
	body, err := json.Marshal(opts)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", fmt.Sprintf("%s%s", svc.client.baseURL, userPath), bytes.NewReader(body))
	if err != nil {
		return nil, err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}

	var user *User
	err = mapstructure.Decode(apiR.Resources, &user)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (svc *UserServiceOp) APIKey(ctx context.Context) (string, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s/api_key", svc.client.baseURL, userPath), nil)
	if err != nil {
		return "", err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return "", err
	}

	key := apiR.Resources.(string)

	return key, nil
}

func (svc *UserServiceOp) List(ctx context.Context) ([]*User, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.baseURL, userPath), nil)
	if err != nil {
		return nil, err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}

	var users []*User
	err = mapstructure.Decode(apiR.Resources, &users)
	if err != nil {
		return nil, err
	}

	return users, nil
}

func (svc *UserServiceOp) WhoAmI(ctx context.Context) (*User, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s/whoami", svc.client.baseURL, userPath), nil)
	if err != nil {
		return nil, err
	}
	var apiR APIResponse
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}

	var u User
	err = mapstructure.Decode(apiR.Resources, &u)
	if err != nil {
		return nil, err
	}

	return &u, nil
}

type User struct {
	Username string  `json:"username,omitempty"`
	Admin    bool    `json:"admin,omitempty"`
	Disabled bool    `json:"disabled,omitempty"`
	ReadOnly bool    `json:"read_only,omitempty"`
	Email    *string `json:"email,omitempty"`
	ID       string  `json:"id,omitempty"`
	MaxTLP   string  `json:"max_tlp,omitempty"`
}
