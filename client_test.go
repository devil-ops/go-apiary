package apiary

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/require"
)

var (
	srv *httptest.Server
	tc  *Client
)

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	shutdown()
	os.Exit(code)
}

func shutdown() {
	srv.Close()
}

func setup() {
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	log.Logger = log.With().Caller().Logger().Output(zerolog.ConsoleWriter{Out: os.Stderr})
	srv = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasSuffix(r.URL.Path, "users/whoami") {
			fakeRespFile(w, "./testdata/users/whoami.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/collections/fake-collection-uuid") && r.Method == "GET" {
			fakeRespFile(w, "./testdata/collections/get.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/collections/fake-collection-uuid/block.txt") && r.Method == "GET" {
			fakeRespFile(w, "./testdata/collections/text.txt")
			return
		} else if strings.HasSuffix(r.URL.Path, "/collections/fake-collection-uuid/block.json") && r.Method == "GET" {
			fakeRespFile(w, "./testdata/collections/block.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/collections/fake-collection-uuid") && r.Method == "DELETE" {
			fakeRespFile(w, "./testdata/collections/delete.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/collections/fake-collection-uuid") && r.Method == "PATCH" {
			fakeRespFile(w, "./testdata/collections/update.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/collections") && r.Method == "GET" {
			fakeRespFile(w, "./testdata/collections/list.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/collections") && r.Method == "POST" {
			fakeRespFile(w, "./testdata/collections/create.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/status") {
			fakeRespFile(w, "./testdata/status/status.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/actions") {
			fakeRespFile(w, "./testdata/actions/list.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/actions/fake-uuid") && r.Method == "GET" {
			fakeRespFile(w, "./testdata/actions/get.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/actions/fake-uuid") && r.Method == "DELETE" {
			fakeRespFile(w, "./testdata/actions/delete.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/actions/fake-uuid") && r.Method == "PATCH" {
			fakeRespFile(w, "./testdata/actions/update.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/observables") {
			fakeRespFile(w, "./testdata/observables/list.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/observables") && r.Method == "POST" {
			fakeRespFile(w, "./testdata/observables/create.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/observables/upload") && r.Method == "POST" {
			fakeRespFile(w, "./testdata/observables/create.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/users/api_key") {
			fakeRespFile(w, "./testdata/users/api_key.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/users/fake-user-uuid") && r.Method == "GET" {
			fakeRespFile(w, "./testdata/users/get.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/users/fake-user-uuid") && r.Method == "DELETE" {
			fakeRespFile(w, "./testdata/users/delete.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/users/fake-user-uuid") && r.Method == "PATCH" {
			fakeRespFile(w, "./testdata/users/update.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/users") && r.Method == "GET" {
			fakeRespFile(w, "./testdata/users/users.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/users") && r.Method == "POST" {
			fakeRespFile(w, "./testdata/users/create.json")
			return
		} else if strings.HasSuffix(r.URL.Path, "/token") {
			ar := AuthResponse{
				AccessToken: "fakey-token",
				TokenType:   "auth",
			}
			b, err := json.Marshal(ar)
			panicIfError(err)
			_, err = io.Copy(w, bytes.NewReader(b))
			panicIfError(err)
			return
		} else {
			log.Warn().Str("url", r.URL.String()).Msg("unexpected request")
			w.WriteHeader(http.StatusNotFound)
		}
	}))
	var err error
	tc, err = NewClient(ClientConfig{
		BaseURL: srv.URL,
		Auth: AuthOpts{
			Username: "foo",
			Password: "bar",
		},
	}, nil)
	panicIfError(err)
}

func TestNewClient(t *testing.T) {
	got, err := NewClient(ClientConfig{
		BaseURL: srv.URL,
		Auth: AuthOpts{
			Username: "foo",
			Password: "bar",
		},
	}, nil)
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestNewClientMissingURL(t *testing.T) {
	got, err := NewClient(ClientConfig{
		Auth: AuthOpts{
			Username: "foo",
			Password: "bar",
		},
	}, nil)
	require.Error(t, err)
	require.Nil(t, got)
	require.EqualError(t, err, "must set baseURL in config")
}

func TestNewClientMissingAuth(t *testing.T) {
	got, err := NewClient(ClientConfig{
		BaseURL: "http://localhost:8000",
	}, nil)
	require.Error(t, err)
	require.Nil(t, got)
	require.EqualError(t, err, "must set username in auth")
}

func fakeRespFile(w io.Writer, f string) {
	rp, err := os.ReadFile(f)
	panicIfError(err)
	_, err = io.Copy(w, bytes.NewReader(rp))
	panicIfError(err)
}
