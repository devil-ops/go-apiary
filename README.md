# Go Apiary

Client library and tool for interacting with Apiary

## ApiaryCTL

This is a command line tool distributed as part of the library packages. You can
use it to interact with any Apiary service.

![demo](docs/apiaryctl.gif)

### Installation

Download binaries from the [release page](https://gitlab.oit.duke.edu/devil-ops/go-apiary/-/releases)

You can also pull from the normal devil-ops locations...check out the
[installing-devil-ops-packages](https://gitlab.oit.duke.edu/devil-ops/installing-devil-ops-packages)
for more info.

The package name to use for yum/homebrew/etc is `apiaryctl`

### Authorization

Auth can be done a number of ways, but the quick and dirty way is:

```shell
$ export APIARY_URL=https://yourapiary-instance
...
$ export APIARY_AUTH_PASSWORD=some-password
...
$ export APIARY_AUTH_USERNAME=admin
...
```
