package apiary

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/mitchellh/mapstructure"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

type CollectionService interface {
	List(context.Context) ([]*Collection, error)
	Get(context.Context, string) (*Collection, error)
	CollectPlainText(context.Context, string, string) ([]byte, error)
	Collect(context.Context, string, string) ([]*ObservableItem, error)
	Delete(context.Context, string) (*Collection, error)
	Create(context.Context, CollectionCreateOpts) (*Collection, error)
	Update(context.Context, string, CollectionUpdateOpts) (*Collection, error)
}

type CollectionServiceOp struct {
	client *Client
}

const collectionPath = "/collections"

func NewCollectionUpdateOptsWithViper(v *viper.Viper) (*CollectionUpdateOpts, error) {
	var opts CollectionUpdateOpts
	err := v.Unmarshal(&opts)
	if err != nil {
		return nil, err
	}
	return &opts, nil
}

func NewCollectionAddOptsWithViper(v *viper.Viper) (*CollectionCreateOpts, error) {
	var opts CollectionCreateOpts
	err := v.Unmarshal(&opts)
	if err != nil {
		return nil, err
	}
	return &opts, nil
}

func collectionsWithInterface(v interface{}) ([]*Collection, error) {
	items := v.([]interface{})
	r := make([]*Collection, len(items))
	// TODO: This seems non-optimal...need a better way to handle this
	for idx, item := range items {
		ri, err := collectionWithInterface(item)
		if err != nil {
			return nil, err
		}
		r[idx] = ri
	}
	return r, nil
}

func collectionWithInterface(v interface{}) (*Collection, error) {
	if v == nil {
		return nil, errors.New("nil interface passed")
	}
	b, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}
	var ret *Collection
	err = json.Unmarshal(b, &ret)
	if err != nil {
		return nil, err
	}
	return ret, nil
}

func (svc *CollectionServiceOp) Update(ctx context.Context, id string, opts CollectionUpdateOpts) (*Collection, error) {
	body, err := json.Marshal(opts)
	if err != nil {
		return nil, err
	}
	log.Debug().Interface("opts", opts).Str("body", string(body)).Send()
	req, err := http.NewRequest("PATCH", fmt.Sprintf("%s%s/%s", svc.client.baseURL, collectionPath, id), bytes.NewReader(body))
	if err != nil {
		return nil, err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}
	item, err := collectionWithInterface(apiR.Resources)
	if err != nil {
		return nil, err
	}

	return item, nil
}

func (svc *CollectionServiceOp) Create(ctx context.Context, opts CollectionCreateOpts) (*Collection, error) {
	body, err := json.Marshal(opts)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", fmt.Sprintf("%s%s", svc.client.baseURL, collectionPath), bytes.NewReader(body))
	if err != nil {
		return nil, err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}
	item, err := collectionWithInterface(apiR.Resources)
	if err != nil {
		return nil, err
	}

	return item, nil
}

func (svc *CollectionServiceOp) List(ctx context.Context) ([]*Collection, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.baseURL, collectionPath), nil)
	if err != nil {
		return nil, err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}

	r, err := collectionsWithInterface(apiR.Resources)
	if err != nil {
		return nil, err
	}

	return r, nil
}

func (svc *CollectionServiceOp) Delete(ctx context.Context, id string) (*Collection, error) {
	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s%s/%s", svc.client.baseURL, collectionPath, id), nil)
	if err != nil {
		return nil, err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}

	collection, err := collectionWithInterface(apiR.Resources)
	if err != nil {
		return nil, err
	}
	return collection, nil
}

func (svc *CollectionServiceOp) Get(ctx context.Context, id string) (*Collection, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s/%s", svc.client.baseURL, collectionPath, id), nil)
	if err != nil {
		return nil, err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}

	collection, err := collectionWithInterface(apiR.Resources)
	if err != nil {
		return nil, err
	}
	return collection, nil
}

func (svc *CollectionServiceOp) Collect(ctx context.Context, id string, action string) ([]*ObservableItem, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s/%s/%s.json", svc.client.baseURL, collectionPath, id, action), nil)
	if err != nil {
		return nil, err
	}
	var apiR APIResponseComplexErrors
	_, err = svc.client.sendRequest(req, &apiR)
	if err != nil {
		return nil, err
	}

	var ois []*ObservableItem
	err = mapstructure.Decode(apiR.Resources, &ois)
	if err != nil {
		return nil, err
	}
	return ois, nil
}

func (svc *CollectionServiceOp) CollectPlainText(ctx context.Context, id, action string) ([]byte, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s/%s/%s.txt", svc.client.baseURL, collectionPath, id, action), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Accept", "text/plain")
	var res DataHolder
	_, err = svc.client.sendRequest(req, &res)
	if err != nil {
		return nil, err
	}

	return []byte(res.Data), nil
}

type Collection struct {
	Comment           string    `json:"comment,omitempty"`
	ID                string    `json:"id,omitempty"`
	MinimumConfidence float64   `json:"minimum_confidence,omitempty"`
	Owners            []string  `json:"owners,omitempty"`
	Patterns          []Pattern `json:"patterns,omitempty"`
	Sources           []Source  `json:"sources,omitempty"`
	Subscribers       []string  `json:"subscribers,omitempty"`
	TLP               TLP       `json:"tlp,omitempty"`
}

type (
	CollectionUpdateOpts struct {
		Comment           string    `json:"comment,omitempty" mapstructure:"comment,omitempty"`
		MinimumConfidence uint      `json:"minimum_confidence,omitempty" yaml:"minimum_confidence" mapstructure:"minimum-confidence,omitempty"`
		Owners            []string  `json:"owners,omitempty" mapstructure:"owner,omitempty"`
		Patterns          []Pattern `json:"patterns,omitempty" mapstructure:"pattern,omitempty"`
		Sources           []Source  `json:"sources,omitempty" mapstructure:"source,omitempty"`
		Subscribers       []string  `json:"subscribers,omitempty" mapstructure:"subscriber,omitempty"`
		TLP               string    `json:"tlp,omitempty"`
	}
	CollectionCreateOpts struct {
		Comment           string    `json:"comment,omitempty"`
		MinimumConfidence uint      `json:"minimum_confidence,omitempty" yaml:"minimum_confidence" mapstructure:"minimum-confidence"`
		Owners            []string  `json:"owners,omitempty" mapstructure:"owner"`
		Patterns          []Pattern `json:"patterns,omitempty" mapstructure:"pattern"`
		Sources           []Source  `json:"sources,omitempty" mapstructure:"source"`
		Subscribers       []string  `json:"subscribers,omitempty" mapstructure:"subscriber"`
		TLP               string    `json:"tlp,omitempty"`
	}
)

type (
	Pattern string
	Source  string
)

type DataHolder struct {
	Data string
}
