package apiary

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"github.com/google/go-querystring/query"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"moul.io/http2curl"
)

type Client struct {
	client     *http.Client
	UserAgent  string
	Config     ClientConfig
	baseURL    string
	token      string
	User       UserService
	Status     StatusService
	Action     ActionService
	Observable ObservableService
	Collection CollectionService
	// Asset     AssetService
	// VRF       VRFService
	// Device    DeviceService
	// IP        IPService
	// Location  LocationService
	// Volume    VolumeService
}

type ClientConfig struct {
	Auth    AuthOpts `json:"auth"`
	BaseURL string   `json:"base_url"`
}

type Response struct {
	*http.Response
}

type ErrorResponse struct {
	Message string `json:"errors"`
}

func (c *Client) auth(a *AuthOpts) error {
	body := a.URLEncode()
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/token", c.baseURL), strings.NewReader(body))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Accept", "application/json; charset=utf-8")

	if os.Getenv("PRINT_CURL") != "" {
		command, _ := http2curl.GetCurlCommand(req)
		fmt.Fprint(os.Stderr, command.String()+"\n")
	}

	res, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	b, _ := ioutil.ReadAll(res.Body)
	var ar AuthResponse
	err = json.Unmarshal(b, &ar)
	if err != nil {
		return err
	}

	c.token = ar.AccessToken
	return nil
}

func (c *Client) sendRequest(req *http.Request, v interface{}) (*Response, error) {
	// Set the content-type if we haven't set it already
	if req.Header.Get("Content-Type") == "" {
		req.Header.Set("Content-Type", "application/json; charset=utf-8")
	}
	if req.Header.Get("Accept") == "" {
		req.Header.Set("Accept", "application/json; charset=utf-8")
	}

	/*
		if c.token == "" {
			log.Debug().Msg("Authenticating now for oauth token")
			err := c.auth(&c.Config.Auth)
			if err != nil {
				return nil, err
			}
		}
	*/
	req.Header.Add("Authorization", "Bearer "+c.token)

	// log.Fatal().Str("content-type", req.Header.Get("Content-Type")).Send()
	if os.Getenv("PRINT_CURL") != "" {
		command, _ := http2curl.GetCurlCommand(req)
		fmt.Fprint(os.Stderr, command.String()+"\n")
	}

	res, err := c.client.Do(req)
	req.Close = true
	if err != nil {
		log.Warn().Err(err).Msg("Error submitting request")
		return nil, err
	}
	// b, _ := ioutil.ReadAll(res.Body)

	defer res.Body.Close()

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes ErrorResponse
		b, _ := ioutil.ReadAll(res.Body)
		log.Debug().Str("response", string(b)).Msg("Response")
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return nil, errors.New(errRes.Message)
		}

		if res.StatusCode == http.StatusTooManyRequests {
			return nil, fmt.Errorf("too many requests.  Check rate limit and make sure the userAgent is set right")
		} else if res.StatusCode == http.StatusNotFound {
			return nil, fmt.Errorf("that entry was not found, are you sure it exists?")
		} else {
			// log.Warn().Str("response", string(b)).Msg("Err")
			log.Warn().RawJSON("response", b).Msg("Error while sending request")
			return nil, fmt.Errorf("error, status code: %d", res.StatusCode)
		}
	}

	b, _ := ioutil.ReadAll(res.Body)
	if string(b) != "" {
		/*
			if err = json.NewDecoder(bytes.NewReader(b)).Decode(&v); err != nil {
				return nil, err
			}
		*/
		if req.Header.Get("Accept") != "text/plain" {
			if err = json.NewDecoder(bytes.NewReader(b)).Decode(&v); err != nil {
				return nil, err
			}
		} else {
			// TODO: This is really gross...is there a better way for plain text?
			// Prolly just use curl instead of the go app for this text stuff
			holder := `{"data": "` + strings.ReplaceAll(string(b), "\n", "\\n") + `"}`
			if err = json.NewDecoder(bytes.NewReader([]byte(holder))).Decode(&v); err != nil {
				return nil, err
			}
		}
	} else {
		// When there is no body
		return nil, nil
	}
	// Set interface to the thing we just got
	r := &Response{res}

	return r, nil
}

func NewClientWithViper(v *viper.Viper, httpClient *http.Client) (*Client, error) {
	url := v.GetString("url")
	username := v.GetString("auth-username")
	password := v.GetString("auth-password")
	c := ClientConfig{
		BaseURL: url,
		Auth: AuthOpts{
			Username: username,
			Password: password,
		},
	}
	return NewClient(c, httpClient)
}

func NewClient(config ClientConfig, httpClient *http.Client) (*Client, error) {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	// Validate Auth pieces
	if config.Auth.Username == "" {
		return nil, errors.New("must set username in auth")
	}
	if config.Auth.Password == "" {
		return nil, errors.New("must set password in auth")
	}
	if config.BaseURL == "" {
		return nil, errors.New("must set baseURL in config")
	}

	userAgent := "go-apiary"
	c := &Client{
		Config:    config,
		client:    httpClient,
		UserAgent: userAgent,
		baseURL:   config.BaseURL,
	}
	c.User = &UserServiceOp{client: c}
	c.Status = &StatusServiceOp{client: c}
	c.Action = &ActionServiceOp{client: c}
	c.Observable = &ObservableServiceOp{client: c}
	c.Collection = &CollectionServiceOp{client: c}

	// Go ahead and get a token please
	log.Debug().Msg("Authenticating now for oauth token")
	err := c.auth(&c.Config.Auth)
	if err != nil {
		return nil, err
	}

	return c, nil
}

type AuthOpts struct {
	GrantType    string `json:"grant_type,omitempty" url:"grant_type"`
	Username     string `json:"username" url:"username"`
	Password     string `json:"password" url:"password"`
	Scope        string `json:"scope,omitempty" url:"scope"`
	ClientId     string `json:"client_id,omitempty" url:"client_id"`
	ClientSecret string `json:"client_secret,omitempty" url:"client_secret"`
}

func (a *AuthOpts) URLEncode() string {
	v, _ := query.Values(a)
	return v.Encode()
}

type AuthResponse struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
}

type ResponseMetadata struct {
	ResourceCount float64 `json:"resource_count,omitempty"`
	TimeTaken     float64 `json:"time_taken,omitempty"`
}

type APIResponse struct {
	Errors    []string         `json:"errors,omitempty"`
	Metadata  ResponseMetadata `json:"metadata,omitempty"`
	Resources interface{}      `json:"resources,omitempty"`
}

type APIResponseComplexErrors struct {
	Errors []struct {
		Message string `json:"message"`
	} `json:"errors,omitempty"`
	Metadata  ResponseMetadata `json:"metadata,omitempty"`
	Resources interface{}      `json:"resources,omitempty"`
}
