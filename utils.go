package apiary

import (
	"net/http"
	"os"
)

func panicIfError(err error) {
	if err != nil {
		panic(err)
	}
}

func GetFileContentType(ouput *os.File) (string, error) {
	// to sniff the content type only the first
	// 512 bytes are used.

	buf := make([]byte, 512)

	_, err := ouput.Read(buf)
	if err != nil {
		return "", err
	}

	// the function that actually does the trick
	contentType := http.DetectContentType(buf)

	return contentType, nil
}
